# Teste prático

Esse teste prático visa avaliar os conhecimentos do candidato a vaga de programador PHP 100% Remoto.

## Objetivos
  - Conhecer um pouco de suas habilidades em:
    - Laravel 5;
    - Entendimento e análise dos requisitos;
    - Capacidade de inovar;
    - Determinação na busca de soluções;
    - Responsabilidade na tomada de decisões.
    
## Escopo
Deve-se criar uma aplicação em PHP para resolver o problema descrito abaixo, utilizando framework Laravel 5. 
Fique a vontade para explorar todo o seu conhecimento em automação de tarefas, CSS e Javascript com JQuery ou qualquer
 outra ferramenta.
 

## Cenário fictício
A Netzee vai lançar um novo blog. Nesse blog, desejamos cadastrar categorias e posts através de um painel 
administrativo.
 

## Requisitos
- [x] Um post pode ter mais de uma categoria.
- [x] A consulta pelo nome é requisito funcional.
- [x] É necessário autenticação.


#### CRUD de Categorias
Criar um gerenciamento aonde seja possível Criar, Listar, Editar e Visualizar uma categoria (Design, Programação, 
Marketing, por exemplo). 

##### Atributos de uma Categoria:
- [x] título (obrigatório)
- [x] status (obrigatório, ativo/inativo)
- [x] descrição


#### CRUD de Posts
Criar um gerenciamento aonde seja possível Criar, Listar, Editar e Visualizar um post. 

##### Atributos de um Post:
- [x] título (obrigatório)
- [x] status (obrigatório, ativo/inativo)
- [x] descrição (obrigatório)
- [ ] imagem de capa


## Instruções:

- [x] No Bitbucket, faça fork desse repositório e adicione a conta "netzee-admin" ou "contato@netzee.com.br" para 
visualizar o mesmo. (Fica em Settings > User and group access > Users)
- [x] Deve ser utilizado o Laravel como framework PHP
- [x] Deve ser utilizado o Composer para gerenciar as dependências da aplicação. 
- [x] Crie um README com orientações para a instalação.


## Plus ++ 
- [ ] Cubra pelo menos 3 recursos de seu código com testes.
- [x] Utilize as melhores práticas da Orientação a Objetos.
- [x] As tabelas do banco de dados criadas através de migrations.
- [x] Utilizar Seeds para dados fictícios.
- [x] Utilizar Docker (caso utilize subir as configurações utilizadas no repositório).
- [x] Utilizar Redis para session.


## Observações:

- O que será avaliado é a qualidade do código não a velocidade de desenvolvimento. Portanto, qualquer 
generator / scaffolding de CRUD, MVC, etc, torna-se desnecessário. 
- Se não for possível terminar todas as funcionalidades, não tem problema.
- Não precisa ser complexo, com varias lib’s e etc. O legal é usar o necessário para ter um código de qualidade
 e de fácil evolução. 
- Lembrando código de qualidade, você pode e deve fazer o que achar necessário para isso, mesmo que não esteja listado
 aqui. 

## Docker
Foi utilizado o projeto [laradock.io](http://laradock.io). Que fornece um ambiente de desenvolvimento já configurado
e de facil
expansão.

## Instalação
Entre na pasta `laradock` e renomeie `env-example` para `.env`.
> cp env-example .env

Na raiz do projeto, renomeie `.env.example` para `.env`.
> cp .env.example .env

Instalação das lib via composer:
> composer install

Compilar e subir o ambiente Docker:
> cd laradock
<br>
> docker-compose up -d workspace nginx mysql phpmyadmin redis

Dependendo da internet e do computador, esse comando pode demorar um pouco, então, hora do café.

Após compilar e subir todo o ambiente Dokcer, e ainda na pasta `laradock`, rode as migrations e seed com o comando:
> docker-compose exec workspace php artisan migrate:fresh --seed

Após o terminco, acessar o sistema em `http://localhost`

Dados de acesso
> email: admin@netzee.com.br
<br>
> senha: admin

## Observaçoẽs
Não deu tempo de fazer os 3 teste como pediu e o upload da foto do post, trabalhei o final de semana todo e não tive tempo para finalizar. 
