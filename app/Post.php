<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property boolean $status
 * @property string $image
 */
class Post extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'status', 'image'];

    /**
     * The roles that belong to the user.
     */
    public function Categories()
    {
        return $this->belongsToMany(Category::class,'posts_has_categories')->withPivot('post_id', 'category_id');
    }

}
