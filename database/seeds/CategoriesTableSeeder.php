<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'Design',
            'description' => 'Design e interfaces HTML',
            'status' => '1',
        ]);

        DB::table('categories')->insert([
            'title' => 'Programação',
            'description' => 'Programação PHP',
            'status' => '1',
        ]);

        DB::table('categories')->insert([
            'title' => 'Marketing',
            'description' => 'Marketing Digital',
            'status' => '1',
        ]);
    }
}
