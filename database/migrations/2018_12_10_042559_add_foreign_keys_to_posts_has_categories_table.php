<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPostsHasCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('posts_has_categories', function(Blueprint $table)
		{
			$table->foreign('category_id', 'fk_posts_has_categories_categories1')->references('id')->on('categories')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('post_id', 'fk_posts_has_categories_posts')->references('id')->on('posts')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('posts_has_categories', function(Blueprint $table)
		{
			$table->dropForeign('fk_posts_has_categories_categories1');
			$table->dropForeign('fk_posts_has_categories_posts');
		});
	}

}
