<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsHasCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts_has_categories', function(Blueprint $table)
		{
			$table->integer('post_id')->unsigned()->index('fk_post_has_category_post_idx');
			$table->integer('category_id')->unsigned()->index('fk_post_has_category_category_idx');
			$table->primary(['post_id','category_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts_has_categories');
	}

}
