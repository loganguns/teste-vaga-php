@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Nova Categoria</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div>
                            <form method="post" action="{{ route('categories.store') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Título *</label>
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Descrição *</label>
                                    <textarea class="form-control" name="description" rows="3"></textarea>
                                </div>

                                <div class="form-group">
                                    <div class="form-check form-check-inline">Status *</div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Ativo</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0">
                                        <label class="form-check-label" for="inlineRadio2">Inativo</label>
                                    </div>
                                </div>
                                <div>
                                    <input type="submit" class="btn btn-primary" value="Salvar">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
