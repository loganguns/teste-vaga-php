@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Pesquisar</div>

                    <div class="card-body">

                        <div>
                            <form method="get" action="{{ route('posts.search') }}">
                                @csrf
                                <input type="text" name="search" placeholder="Ex.: Olá Mundo">
                                <input type="submit" class="btn btn-sm">
                            </form>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">Todos os Posts</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <div>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Título</th>
                                    <th scope="col">Descrição</th>
                                    <th scope="col">Categorias</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Opçoes</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <th scope="row">{{ $post->id }}</th>
                                        <td>{{ $post->title }}</td>
                                        <td>{{ $post->description }}</td>
                                        <td>
                                            @foreach($post->categories as $category)
                                                {{ $category->title }}
                                            @endforeach
                                        </td>
                                        <td>
                                            @if($post->status == 1)
                                                Ativo
                                            @elseif($post->status == 0)
                                                Inativo
                                            @endif
                                        </td>
                                        <td><a href="{{ route('posts.show', $post->id) }}">Editar</a> - Exluir</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
